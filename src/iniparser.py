import re

class Config():
    def __init__(self, file=None, path=None):
        self.file = file or (path and open(path))
        self.data = (self.file and self._load(self.file)) or {}


    @staticmethod
    def _numberize(value):
        try:
            value = int(value)
        except ValueError:
            try:
                value = float(value)
            except ValueError:
                pass

        return value

    def _load(self, file):
        rez = {}
        current_section = None

        for line in file:
            if line[0] == ';': continue

            section = re.search('\[(.*)\]', line)
            if section:
                current_section = section.group(1)
                rez[current_section] = {}
                continue

            pattern = r"\s*(?P<key>.*)=\s*(('(?P<string>.*)')|(?P<string2>.*))"
            match = re.search(pattern, line)

            if match:
                value = self._numberize(match.group('string') or match.group('string2'))
                key = match.group('key')

                if current_section:
                    rez[current_section][key] = value
                else:
                    rez[key] = value

        return rez

    def parse(self):
        return self.data

    def get_sections(self):
        return [sect for sect, vals in self.data.items() if isinstance(vals, dict)]

    def get_for_section(self, section):
        return [pair for pair in self.data[section].items()]

    def get_global_values(self):
        return [pair for pair in self.data.items() if not isinstance(pair[1], dict)]

    def add_section(self, section):
        self.data[section] = {}

    def set_key(self, key, value, section=None):
        if section:
            self.data[section][key] = value
        else:
            self.data[key] = value

    def del_section(self, section):
        del self.data[section]

    def save(self, file=None):
        file = file or self.file

        for key, value in self.get_global_values():
            if isinstance(value, str):
                file.write("{}='{}'\n".format(key, value))
            else:
                file.write("{}={}\n".format(key, value))

        for sect in self.get_sections():
            file.write('\n[{}]\n'.format(sect))
            for key, value in self.get_for_section(sect):
                file.write(("{}='{}'\n" if isinstance(value, str) else "{}={}\n").format(key, value))

    def __getitem__(self, item):
        return self.data[item]

    def __setitem__(self, key, value):
        self.data[key] = value

    def __delitem__(self, key):
        del self.data[key]