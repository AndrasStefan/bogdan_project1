import sys
import time


DEBUG = 0
INFO = 1
WARNING = 2
ERROR = 3

_MAP = {
    DEBUG: 'DEBUG',
    INFO: 'INFO',
    'INFO': INFO,
}

_levels = {DEBUG, INFO, WARNING, ERROR}


class MyLogger(object):
    def __init__(self, name):
        # self.file = sys.stdout
        self.level = DEBUG
        self.name = name

    def config(self, level=DEBUG, **config):
        self.level = level

    def log(self, message, level):
        print('[{}  {}  <{}>  {}] {}\n'.format(time.strftime('%x'), time.strftime('%X'), self.name, level, message))

    def info(self, message):
        if self.level <= INFO:
            self.log(message, 'INFO')

    def debug(self, message):
        if self.level <= DEBUG:
            self.log(message, 'DEBUG')

    def warning(self, message):
        if self.level <= WARNING:
            self.log(message, 'WARNING')

    def error(self, message):
        if self.level <= ERROR:
            self.log(message, 'ERROR')

    def set_level(self, newlvl):
        if newlvl in _levels:
            self.level = newlvl

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()


class FileLogger(MyLogger):
    def __init__(self, name, file=None, path=None, level=DEBUG):
        super().__init__(name)
        if path: self.file = open(path, 'a')
        if file: self.file = file
        self.level = level

















