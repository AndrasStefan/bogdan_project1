from src import funcs
from src import io
from src import intro


def print_menu():
    print('1. Reverse + upper')
    print('2. Check if a number is palindrome')
    print('3. Print all the palindrome numbers lower than n')
    print('-' * 4 + ' Funcs ' + '-' * 4)
    print('4. Check if a number is prime')
    print('5. Fibonacci numbers up to ur numba, iterative')
    print('6. Fibonacci nubmers up to ur numba, recursive')
    print('7. Sort a list o values (input: 5 1 2 3 4 5 10)')
    print('-' * 4 + ' Input/output' + '-' * 4)
    print('8. Sum of all numbers in a file')
    print('9. All primes lower than the number stored in primeinput')
    print('10. All words used only once in a file')
    print('11. Extract all the links on a given url')
    print('12. Reads a number of file containing sorted numbers, and outputs a large file with all')
    print('13. Read from a file, with a buffer')
    print('14. Advanced reader')

    print
    print('0. QUIT')


def merge_files(files):
    rez = []
    for file in files.split():
        l = []
        with open(file) as f:
            for line in f:
                l.append(int(line))
        rez = funcs.merge(rez, l)
    return rez


def print_merge(all):
    with open('result', 'w') as f:
        f.writelines(all)


while True:
    print_menu()
    cmd = input('Command: ')

    if cmd == '1':
        name = input('What is ur name ?   ')
        print(intro.upper_reversed(name))
    elif cmd == '2':
        number = input('Which numba ?   ')
        print(intro.is_palindrome(number))
    elif cmd == '3':
        number = input('Palinds lower than ?   ')
        print(intro.pal_lower_than(int(number)))
    elif cmd == '4':
        number = input('Which numba ?   ')
        print(funcs.is_prime(int(number)))
    elif cmd == '5':
        number = input('Which numba ?   ')
        print(funcs.fib_iterativ(int(number)))
    elif cmd == '6':
        number = input('Which numba ?   ')
        print(funcs.fib_recurs(int(number)))
    elif cmd == '7':
        numbers = input('The list is .. ?   ')
        l = [int(elem) for elem in numbers.split()]
        print(funcs.merge_sort(l))
    elif cmd == '8':
        path = input('Which file ?    ')
        print(io.sum_file(path=path))
    elif cmd == '9':
        with open('primeinput') as f:
            number = f.readline()
        rez = funcs.prime_lower_than(int(number))
        for nr in rez:
            print(nr)
    elif cmd == '10':
        file = input('Which file ?    ')
        print(io.words_only_once(file))
    elif cmd == '11':
        print(io.all_links2('https://www.evozon.com/'))
    elif cmd == '12':
        files = input('Files    ')
        rez = merge_files(files)
        print_merge(rez)
    elif cmd == '13':
        with open('text') as f:
            for x in funcs.file_reader(f, 4):
                print(x)
    elif cmd == '14':

        myreader = funcs.reader(open('text'))

        for data in myreader.send(2):
            print(data)
            #if data == 'ci': break
            if 't' in data: break

        for data in myreader.send(4):
            print(data)


    elif cmd == '0':
        break
    else:
        print('Command is not supported.\n')

    print

