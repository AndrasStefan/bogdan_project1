import time

time_history = []

def measure_total_time(func):
    print("AM INTRAT AICI")
    l = time_history
    def wrap(*args):
        print("DEC's closure")
        start = time.time()
        ret = func(*args)
        end = time.time()
        t = end - start
        l.append( (func.__name__, t) )
        total = 0
        print
        for elem in l:
            print(str(elem[0]) + ' took: ' + str(format(elem[1], '.5f')))
            total += elem[1]
        print('Total time: ' + str(format(total, '.5f')))
        print
        return ret
    return wrap