from src import logger


l = ['red', 'black', 'green', 'adsf']

fisier = open('numere1', 'w')
mylog3 = logger.FileLogger(file=fisier, name='user1')


def fun1():
    mylog1 = logger.FileLogger(file=fisier, level=logger.DEBUG, name='user3')
    for c, elem in enumerate(l):
        mylog1.debug('Index: ' + str(c) + ' and elem: ' + str(elem))
        if c > 2:
            mylog1.info('Sunt mai mult de 2 culori aici')


class Klass1():
    def __init__(self, logge):
        self.logger = logge
        self.logger.config()
        self.logger.set_level(logger.DEBUG)

    def foo(self):
        for c, elem in enumerate(l):
            self.logger.debug('Index: ' + str(c) + ' and elem: ' + str(elem))
            if c > 2:
                self.logger.info('Sunt mai mult de 2 culori in clasa')


file_main = open('main.log', 'w')
mylog2 = logger.FileLogger(file=file_main, level=logger.DEBUG, name='user2')

second = logger.FileLogger(file=file_main, level=logger.DEBUG, name='stefan')


fun1()
mylog2.info('fun1 terminated')
k1 = Klass1(mylog3)
mylog2.info('k1 instance')
k1.foo()
mylog2.debug('something wrong?')
mylog2.error('404')

console = logger.MyLogger(name='another one')
console.info('new logger initialized')
mylog3.debug('dada')


second.warning('new user found')
second.info('config loaded')
mylog2.warning('2nd user detected')

file_main.close()

with logger.FileLogger(path='main.log', level=logger.DEBUG, name='new') as log:
    log.debug('new message')
    log.info('context manager')

with logger.MyLogger(name='asd') as l2:
    l2.debug('asd')

with logger.FileLogger(path='main.log', level=logger.INFO, name='new2') as log:
    log.info('new user')




