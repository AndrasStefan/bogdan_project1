import re
import urllib
from html.parser import HTMLParser


def sum_file(file=None, path=None):
    if path:
        s = 0
        with open(path) as f:
            for line in f:
                s += int(line)
        return s

    if file:
        s = 0
        for line in file:
            s += int(line)
        return s


def words_only_once(file):
    counter = {}
    with open(file) as f:
        for word in re.findall('\w+', f.read()):
            if word in counter:
                counter[word] += 1
            else:
                counter[word] = 1

    return [word for word,count in counter.items() if count == 1]


def all_links(link):
    site = urllib.urlopen(link)
    html = site.read()


    class MyHTMLParser(HTMLParser):
        href_links = []

        def handle_starttag(self, tag, attrs):
            if tag == 'a':
                for atr in attrs:
                    if atr[0] == 'href' and 'http' in atr[1]:
                        self.href_links.append(atr[1])


    parser = MyHTMLParser()
    parser.feed(html)

    return parser.href_links


def all_links2(link):
    site = urllib.urlopen(link)
    html = site.read()
    return re.findall(r'<a[^>]* href="([^"]*)"', html)






