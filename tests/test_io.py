from io import StringIO

from src import io

from functools import reduce

file_content = u"""1
2
3
4
"""
file = StringIO(file_content)


def test_sum_file():
    assert io.sum_file(path='testsum') == reduce(lambda x, y: x + y, range(0, 100))
    assert io.sum_file(path='testsum2') == reduce(lambda x, y: x + y, range(0, 10 ** 3, 2))
    assert io.sum_file(file=file) == 10

def test_words_only_once():
    assert len(io.words_only_once('testwords')) == 38