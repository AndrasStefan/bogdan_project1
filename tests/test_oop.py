from io import StringIO

from src import iniparser


def test_iniparser():
    file_content = u"""[section1]
    name='stef'
    age=20
    
    [section2]
    name='player'
    age=12
    """
    file = StringIO(file_content)

    ini = iniparser.Config(file=file)

    assert ini.get_sections() == ['section1', 'section2']
    assert ini.get_for_section('section1') == [('name', 'stef'), ('age', 20)]

    ini['section1']['sname'] = 'second'
    assert ini.get_for_section('section1') == [('name', 'stef'), ('age', 20), ('sname', 'second')]

    ini.set_key('author', 'stef')
    assert ini.get_global_values() == [('author', 'stef')]

    del ini['author']
    assert ini.get_global_values() == []

    file.close()